﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserWebApi.Data;
using UserWebApi.Models;
using UserWebApi.Validator;

namespace UserWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserDbContext _userDbContext;
        private readonly UserValidator _userValidator;
        public UserController(UserDbContext userDbContext)
        {
            this._userDbContext = userDbContext;
            _userValidator = new UserValidator();
        }
        /// <summary>
        /// 回傳一個User
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<User>> GetById(int? id,string? name)
        {
            if (id == null && name == null) return BadRequest();
            if (id == null)
            {
                var username= await _userDbContext.Users.FirstOrDefaultAsync(x => x.Name == name);
                return username == null ? BadRequest() : Ok(username);
            }
            if (name == null)
            {
                var userid= await _userDbContext.Users.FirstOrDefaultAsync(x => x.Id == id);
                return userid == null ? BadRequest() : Ok(userid);
            }
            var user = await _userDbContext.Users.FirstOrDefaultAsync(x => x.Id == id && x.Name == name);
            return user == null ? BadRequest() : Ok(user);
        }
        /// <summary>
        /// 如果name已經存在BadRequest()，不存在會新增。
        /// </summary>
        /// <param name="user">用戶資料</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Create(User user) 
        {
            var validationResult = _userValidator.Validate(user);

            if (!validationResult.IsValid)
            {
                return BadRequest(validationResult.Errors);
            }
            var username = await _userDbContext.Users.FirstOrDefaultAsync(x => x.Name == user.Name);
            if (username != null)
            {
                return BadRequest();
            }
            await _userDbContext.Users.AddAsync(user);
            await _userDbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
