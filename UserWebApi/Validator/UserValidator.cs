﻿using FluentValidation;
using UserWebApi.Models;

namespace UserWebApi.Validator;

public class UserValidator : AbstractValidator<User>
{
    public UserValidator()
    {
        RuleFor(user => user.Name)
            .NotEmpty()
            .Matches("^[a-zA-Z0-9]{6,12}$")
            .WithMessage("name 必須是英文或數字,長度6~12");

        RuleFor(user => user.Password)
            .NotEmpty()
            .Matches("^(?=.*[A-Za-z])(?=.*[0-9]).{6,12}$")
            .WithMessage("password 必須同時包含英文和數字,長度6~12");
    }
}

