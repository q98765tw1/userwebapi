﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using UserWebApi.Models;

namespace UserWebApi.Data
{
    public class UserDbContext: DbContext
    {
		public DbSet<User> Users { get; set; }
        public UserDbContext(DbContextOptions<UserDbContext> dbContextOptions) :base(dbContextOptions)
        {
			try
			{
				var databaseCreator = Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator;
				if (databaseCreator != null)
				{
					//資料庫連不到就建一個
					if (!databaseCreator.CanConnect()) databaseCreator.Create();
					//資料表連不到就建一個
					if(!databaseCreator.HasTables()) databaseCreator.CreateTables();
				}
			}
			catch (Exception ex)
			{

				Console.WriteLine(ex.Message);
			}
        }
    }
}
