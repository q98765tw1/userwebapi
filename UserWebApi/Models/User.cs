﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserWebApi.Models
{
    [Table("user")]
    public class User
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string? Name { get; set; }
        [Column("password")]
        public string? Password { get; set; }
        [Column("addtime")]
        public DateTime Addtime { get; set; }
        [Column("updatetime")]
        public DateTime Updatetime { get; set; }
    }
}
